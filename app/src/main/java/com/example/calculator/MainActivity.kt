package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView


class MainActivity : AppCompatActivity() {
    private var operand: Double = 0.0
    private var secondOperand: Double = 0.0
    private var operation: String = ""
    private lateinit var resultTextView: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        resultTextView = findViewById(R.id.resultTextView)

    }

    fun numberClick(clickedView: View) {
        if (clickedView is TextView) {
            var result = resultTextView.text.toString()
            val number = clickedView.text.toString()
            resultTextView.text = result + number

            if (result == "0") {
                result = ""
            }

        }
    }


    fun operationClick(ClickedView: View) {
        if (ClickedView is TextView) {
            val result = resultTextView.text.toString()

            if (result.isNotEmpty()) {
                operand = result.toDouble()
            }
            operation = ClickedView.text.toString()

            resultTextView.text = ""
        }
    }

    fun equalsClick(clickedView: View) {
        val secondOperandText = resultTextView.text.toString()
        var secondOperand: Double = 0.0

        if (secondOperandText.isNotEmpty()) {
            secondOperand = secondOperandText.toDouble()
        }

        when (operation) {
            "+" -> resultTextView.text = (operand + secondOperand).toString()
            "-" -> resultTextView.text = (operand - secondOperand).toString()
            "*" -> resultTextView.text = (operand * secondOperand).toString()
            "÷" -> resultTextView.text = (operand / secondOperand).toString()
            "C" -> resultTextView.text = ""
            "%" -> resultTextView.text = (operand * secondOperand / 100).toString()
            "( )" -> resultTextView.text = ("($operand)").toString()
            "+/-" -> resultTextView.text = ("-$operand").toString()
            "." -> resultTextView.text = ("aqdzagliamarxistavi")

        }
    }
}
